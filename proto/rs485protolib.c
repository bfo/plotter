#include "rs485protolib.h"
#include <stdlib.h>
#include <string.h>

#include "../rs485hwlib-msp430.h"

typedef struct {
	uint8_t myAddress;
	uint8_t currReqByteIndex;
	RS485State state;
	RS485Status recentStatus;
	RS485RequestMessage request;
	uint8_t currRespByteIndex;
	RS485ResponseMessage response;
} RS485Variables;

RS485Variables vars;
#ifdef DEBUG
uint8_t lumpBytesRcvd = 0;
uint8_t lastRcvdBytes[10];
RS485State lastState[10];
bool setNextState = false;
#endif

static RS485Command commandRegistry[RS485CommandRegisterMaxSize];

RS485Status status(const RS485Status status){
	return (vars.recentStatus = status);
}



void stateTransition(void){
	HW_RTSset();
	uint8_t receivedByte = 0;
	if (HW_UARTaddress()){
		vars.state = AWAIT_REQUEST_FETCH_ADDRESS;
#ifdef DEBUG
		lumpBytesRcvd = 0;
#endif
	}
	// When TXC interrupt it may cause some problems and delays
	if(vars.state < VALIDITY_CHECK){
		receivedByte = HW_UARTget();
#ifdef DEBUG
//		lastRcvdBytes[lumpBytesRcvd % 10] = receivedByte;
//		lastState[lumpBytesRcvd % 10] = vars.state;
		lumpBytesRcvd++;
#endif
	}
//		receivedByte = 0;
	switch (vars.state){
	// waiting for an address, and if we reach here, then it means there is an address on a bus
	case AWAIT_REQUEST_FETCH_ADDRESS:
		// pull out the real address info
		vars.request.address = (receivedByte >> 1);
		// get the last bit - REQ_RESP
		vars.request.respond = (receivedByte & 0x01);
		if (vars.request.address == vars.myAddress || vars.request.address == 0){
			HW_QuitMPMode();
			vars.state = AWAIT_REQUEST_FETCH_COMMAND;
#ifdef DEBUG
			setNextState = true;
#endif
		} else {
			HW_EnterMPMode();
		}
		break;
		// got past the address phase, read command
	case AWAIT_REQUEST_FETCH_COMMAND:
		vars.request.command = receivedByte;
		vars.state = AWAIT_REQUEST_FETCH_NBPARAMS;
		break;

		// waiting for number of params byte
	case AWAIT_REQUEST_FETCH_NBPARAMS:
		vars.request.numberOfParameters = receivedByte;
		vars.currReqByteIndex = 0;
		if (vars.request.numberOfParameters == 0)
			vars.state = AWAIT_REQUEST_FETCH_CRC;
		else
			vars.state = AWAIT_REQUEST_FETCH_PARAMS;
		break;
		// waiting for params
	case AWAIT_REQUEST_FETCH_PARAMS:
		vars.request.parameter[vars.currReqByteIndex++] = receivedByte;
		if (vars.currReqByteIndex == vars.request.numberOfParameters)
			vars.state = AWAIT_REQUEST_FETCH_CRC;
		break;
		// waiting for crc
	case AWAIT_REQUEST_FETCH_CRC:
		vars.request.crc8 = receivedByte;
		vars.state = VALIDITY_CHECK;
		//only another command directed at this device can trigger an interrupt, process in peace
		HW_EnterMPMode();
		break;
	case VALIDITY_CHECK:
		break;
	case PROCESSING_REQUEST:
		break;
	case AWAIT_RESPONSE_PUSH_ADDRESS:
		break;
	case AWAIT_RESPONSE_PUSH_COMMAND:
		vars.state = AWAIT_RESPONSE_PUSH_NBDATA;
		HW_UARTsend(vars.response.command);
		break;
	case AWAIT_RESPONSE_PUSH_NBDATA:
		vars.currRespByteIndex = 0;
		if (vars.response.numberOfReturnValues == 0)
			vars.state = AWAIT_RESPONSE_PUSH_CRC;
		else
			vars.state = AWAIT_RESPONSE_PUSH_DATA;
		HW_UARTsend(vars.response.numberOfReturnValues);
		break;
	case AWAIT_RESPONSE_PUSH_DATA:
		if (vars.response.numberOfReturnValues == vars.currRespByteIndex+1)
			vars.state = AWAIT_RESPONSE_PUSH_CRC;
		else
			vars.state = AWAIT_RESPONSE_PUSH_DATA;

		HW_UARTsend(vars.response.returnValue[vars.currRespByteIndex++]);
		break;
	case AWAIT_RESPONSE_PUSH_CRC:
		vars.state = FINISH_TRANSMISSION;
		HW_UARTsend(vars.response.crc8);
		break;
	case FINISH_TRANSMISSION:
		vars.state = AWAIT_REQUEST_FETCH_ADDRESS;
		//disable Tx Interrupt (compatibility issues)
		HW_TxISR_Off();
		HW_EnterMPMode();
		break;
	default:
		break;
	}
	HW_RTSclear();
}

bool rs485MessageAvailable(){
	return VALIDITY_CHECK == vars.state;
}

RS485Status initializeRS485slave(const uint8_t p_myAddress){
	if (0 == p_myAddress)
		return status(ZERO_ADDRESS_NOT_ALLOWED);
	if (127 < p_myAddress)
		return status(ADDRESS_TO_BIG);
	HW_ISRoff();
	HW_UARTinit();
#ifdef DEBUG
	lumpBytesRcvd = 0;
#endif
	vars.myAddress = p_myAddress;
	vars.state = AWAIT_REQUEST_FETCH_ADDRESS;
	HW_EnterMPMode();
	HW_ISRon();
	//Tell the receiver to start transmission
	HW_RTSclear();
	return status(OK);
}

RS485Status consumeRS485Request(){
	if(!rs485MessageAvailable())
		return status(NO_REQUEST_AVAILABLE);
	RS485ResponseMessage response;
	response.numberOfReturnValues = 0;
	// on crc mismatch
	if (computeRequestCrc8(&vars.request) != vars.request.crc8)
	{
		return status(CRC_MISMATCH);
	}

	executeRS485Command(&(vars.request), &response);

	if (vars.request.respond)
	{
		vars.state = AWAIT_RESPONSE_PUSH_ADDRESS;
		respondViaRS485(&response);
	}
	else
	{
		vars.state = AWAIT_REQUEST_FETCH_ADDRESS;
	}

	return status(OK);
}

RS485Status respondViaRS485(const RS485ResponseMessage* p_response)
{
	uint8_t i;
	if (AWAIT_RESPONSE_PUSH_ADDRESS != vars.state){
		return status(NO_RESPONSE_EXPECTED);
	}
	HW_DELAY(); // Give the master a chance to switch from send to receive before we turn the bus around.
	HW_RTSset(); // Set RS485 transceiver to send.
	vars.response.address = p_response->address;
	vars.response.command = p_response->command;
	vars.response.numberOfReturnValues = p_response->numberOfReturnValues;
	for (i = 0; i < vars.response.numberOfReturnValues; i++){
		vars.response.returnValue[i] = p_response->returnValue[i];
	}
	vars.response.crc8 = computeResponseCrc8(&(vars.response));
	vars.currRespByteIndex = 0;
	vars.state = AWAIT_RESPONSE_PUSH_COMMAND;
	//clear interrupt flag and enable Tx interrupt routine (compatibility issues)
	HW_TxISR_On();
	HW_UARTsend(vars.response.address); // Send first byte via RS485. Subsequent bytes are sent by ISR (when needed).
	return status(OK);
}

RS485Status recentRS485Status(){
	return vars.recentStatus;
}

uint8_t computeResponseCrc8(const RS485ResponseMessage* p_msg){
	// allocate space for address, command, and params
	uint8_t i;
	uint8_t temp[3 + MAX_DATABYTES];
	uint8_t length = (3 + p_msg->numberOfReturnValues) * sizeof(uint8_t);
	temp[0] = p_msg->address; //we respond with bare address of responding device without any 'magic' bits
	temp[1] = p_msg->command;
	temp[2] = p_msg->numberOfReturnValues;
	for (i = 0; i < p_msg->numberOfReturnValues; i++)
	{
		temp[3 + i] = p_msg->returnValue[i];
	}
	return genericCrc8(temp, length);
}

uint8_t computeRequestCrc8(const RS485RequestMessage* p_msg){
	// allocate space for address, command, and params
	uint8_t i;
	uint8_t temp[3 + MAX_DATABYTES];
	uint8_t length = (3 + p_msg->numberOfParameters) * sizeof(uint8_t);
	temp[0] = ((p_msg->address) << 1) + p_msg->respond;
	temp[1] = p_msg->command;
	temp[2] = p_msg->numberOfParameters;
	for (i = 0; i < p_msg->numberOfParameters; i++)
	{
		temp[3 + i] = p_msg->parameter[i];
	}
	return genericCrc8(temp, length);
}

// calculate a Dallas CRC8 checksum
uint8_t genericCrc8(const uint8_t* data_pointer, const uint8_t number_of_bytes){
	uint8_t temp1, bit_counter, feedback_bit, crc8_result = 0;
	uint8_t i = number_of_bytes;
	while (i--)
	{
		temp1 = *data_pointer++;

		for (bit_counter = 8; bit_counter; bit_counter--)
		{
			feedback_bit = (crc8_result & 0x01);
			crc8_result >>= 1;
			if (feedback_bit ^ (temp1 & 0x01))
			{
				crc8_result ^= 0x8c;
			}
			temp1 >>= 1;
		}

	}
	return crc8_result;
}

void registerRS485Command(const uint8_t id, const uint8_t type, const RS485CommandHandler handler){
	uint8_t i;
	//find empty space
	for(i=0; i < RS485CommandRegisterMaxSize; i++){
		if (commandRegistry[i].handler == NULL) break;
	}
	//if not found - registration failed
	if (i==RS485CommandRegisterMaxSize) return;

	commandRegistry[i].id = id;
	commandRegistry[i].type = type;
	commandRegistry[i].handler = handler;
}

void unregisterRS485Command(const uint8_t id){
	uint8_t i;
	//find specified command
	for(i=0; i < RS485CommandRegisterMaxSize; i++){
		if (commandRegistry[i].id == id) break;
	}
	//if not found - we fail
	if (i==RS485CommandRegisterMaxSize) return;

	commandRegistry[i].handler = NULL;
	commandRegistry[i].id = 0;
	commandRegistry[i].type = 0;

}
RS485Status executeRS485Command(const RS485RequestMessage* request, RS485ResponseMessage* response){
	// Step 1 find command in registry
	uint8_t i;
	RS485Command* command = NULL;

	for(i=0; i < RS485CommandRegisterMaxSize; i++){
		if (commandRegistry[i].id == request->command) break;
	}
	//if not found - we fail
	if (i==RS485CommandRegisterMaxSize) return status(COMMAND_NOT_FOUND);

	command = commandRegistry + i;

	// Step 2 check type // TODO: This is useless, but whatever
	if (!((request->address == 0 && (command->type & RS485CommandAcceptBroadcast))
			|| (request->address > 0 && (command->type & RS485CommandAcceptDirect))))
		return status(COMMAND_WRONG_TYPE);

	// Step 3 execute command
	command->handler(request, response);

	response->command = request->command;
	response->address = request->address;

	// I don't need to do anything else
	return status(OK);
}

void addResponseValue(RS485ResponseMessage* rsp, uint8_t val){
	if (rsp->numberOfReturnValues < MAX_DATABYTES)
	{
		rsp->returnValue[rsp->numberOfReturnValues] = val;
		rsp->numberOfReturnValues++;
	}
}
