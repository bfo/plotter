#include <msp430g2553.h>
//#include <msp430g2452.h>
//#include <legacymsp430.h>
#include <stdbool.h>

#include "proto/rs485protolib.h"
#include "main-slave-msp430.h"

#define _CLB(BYTE,BIT) BYTE &= ~BIT
#define _STB(BYTE,BIT) BYTE |= BIT
#define _TGB(BYTE,BIT) BYTE ^= BIT
#define RS485_SLAVE_ADDRESS 1
#define RS485_MASTER_ADDRESS 0

timerConfigStruct timerConfig;

uint8_t drift;
uint8_t periods = 0;

inline static void _INIT(void) {
	//setup WDT
	//WDT off
	WDTCTL = WDTPW + WDTHOLD;

	//ACLK - set to LFXT1 = VLO
	BCSCTL3 |= LFXT1S_2;

	//global enable interrupts
	_BIS_SR(GIE);


	//clear OSCFault flag
	//wait for clock fault system to react

	do {
		IFG1 &= ~OFIFG; // Clear OSCFault flag
	} while (BCSCTL3 & LFXT1OF);

	//DCO 1MHz
	if (CALDCO_1MHZ == 0xFF || CALBC1_1MHZ == 0xFF) {
		//calibration data was erased! we can't proceed
		while (true);
	} else {
		BCSCTL1 = CALBC1_1MHZ;
		DCOCTL = CALDCO_1MHZ;
	}

	//MCLK - default DCO/1

	//SMCLK - default DCO/1

	_STB(P1DIR,BIT7);

}

inline static void setupTimers(void) {
	// count up to 20k
	TA0CCR0 = 20000;
	TA0CCR1 = 1500;
	// enable timer interrupts
	TA0CCTL1 = OUTMOD_7;
	// drive timer from SMCLK and count up
	TA0CTL = TASSEL_2 + MC_1 + TAIE;

}

static void lowerThreshold(const RS485RequestMessage* rq, RS485ResponseMessage* rsp){
	uint8_t timerNumber = rq->parameter[0];
	uint16_t timerDiff = (rq->parameter[1] << 8) | rq->parameter[2];

	if( timerConfig.threshold[timerNumber] - timerDiff >= 1500 )
		timerConfig.threshold[timerNumber] -= timerDiff;
}

static void raiseThreshold(const RS485RequestMessage* rq, RS485ResponseMessage* rsp){
	uint8_t timerNumber = rq->parameter[0];
	uint16_t timerDiff = (rq->parameter[1] << 8) | rq->parameter[2];
	if( timerConfig.threshold[timerNumber] + timerDiff <= 2500 )
		timerConfig.threshold[timerNumber] += timerDiff;
}

static void setThreshold(const RS485RequestMessage* rq, RS485ResponseMessage* rsp){
	uint8_t timerNumber = rq->parameter[0];
	uint16_t timerThreshold = (rq->parameter[1] << 8) | rq->parameter[2];
	if( timerThreshold <= 2500 && timerThreshold >= 1500 )
		timerConfig.threshold[timerNumber] = timerThreshold;

}

int main(void) {
	_INIT();

	//TODO: set first servo PWM to 25%
	timerConfig.threshold[0] = 1500;

	drift = 0;

	_STB(P1DIR,BIT6);
	_STB(P1SEL,BIT6);
//	_STB(P1SEL2,BIT6);

	registerRS485Command(1, RS485CommandAcceptDirect, lowerThreshold);
	registerRS485Command(2, RS485CommandAcceptDirect, raiseThreshold);
	registerRS485Command(3, RS485CommandAcceptDirect, setThreshold);

	initializeRS485slave(RS485_SLAVE_ADDRESS);
	setupTimers();

	while (true){
		// If any request is available, try to process
		if (rs485MessageAvailable())
		{
			consumeRS485Request();
		}
	}

	return 0;
}

//Timer interrupt that does real PWM
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_A0(void)
{
	TA0CTL &= ~TAIFG;
	TA0CCR1 = timerConfig.threshold[0]+drift;

//	drift += 100;
//	if (drift > 10000) drift = 100;
}
