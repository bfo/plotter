/*
 * main-slave-msp430.h
 *
 *  Created on: 28-04-2013
 *      Author: bartek
 */

#ifndef MAIN_SLAVE_MSP430_H_
#define MAIN_SLAVE_MSP430_H_

typedef struct {
	//set all thresholds to zero
	uint16_t threshold[10];
} timerConfigStruct;


#endif /* MAIN_SLAVE_MSP430_H_ */
